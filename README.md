React JS

# Aprendiendo React JS
> React JS es una biblioteca de JavaScript para construir interfaces de usuario.


## Agregar React JS através de CDN
```html
<script crossorigin src="https://unpkg.com/react@17/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@17/umd/react-dom.development.js"></script>
```

También se recomienda verificar que el CDN que estás utilizando establece el encabezado HTTP Access-Control-Allow-Origin: *:


![React JS](https://es.reactjs.org/static/89baed0a6540f29e954065ce04661048/13ae7/cdn-cors-header.png)

Esto permite una mejor experiencia en el manejo de errores en React 16 y versiones posteriores.

## Usando React JS

```bash
$ npm -v
$ npm install -g create-react-app
$ npm list
$ create-react-app auditor && cd auditor
$ npm install react-router --save
$ npm run start 
$ firefox http://localhost:3000
```


Enlaces:
* [React JS](https://es.reactjs.org/)
* [Javascript Info](https://javascript.info/)
* [Node JS](https://nodejs.org/es/)
* [Guru 99 React JS](https://www.guru99.com/reactjs-tutorial.html)
