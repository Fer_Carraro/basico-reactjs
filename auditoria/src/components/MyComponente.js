import React,{Component} from 'react';

class MyComponente extends Component{

	render(){

		const estilo={
			backgroundColor:"black",
			color:"orange",
			padding: "5px 15px"
		};

		const estilo_p={
			backgroundColor:"yellow",
			color:"black"
		};

		return(
			<>
			<h4 style={estilo}>Este es un componente React JS: <span className="my-component">MyComponente</span></h4>
			<p style={estilo_p}>Es hora de aprender React JS</p>
			</>
		);
	}
}

export default MyComponente;