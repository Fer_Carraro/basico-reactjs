import logo from './assets/images/logo.svg';
import './App.css';
import MyComponente from './components/MyComponente';



function GetDia(){
  var dia = new Date().getDay();
  var result = "";
  switch(dia){
    case 0: result = "Domingo";break;
    case 6: result = "Sabado";break;
    case 5: result = "Viernes";break;
    case 4: result = "Jueves";break;
    case 3: result = "Miercoles";break;
    case 2: result = "Martes";break;
    case 1: result = "Lunes";break;
    default: result = "NO ESPECIFICADO";break;
  }
  var hoy = "El dia de hoy es "+result;
  return hoy;
}

function HolaMundo(nombre, edad){
  var presentacion = (<div>
  <h2>Hola, {nombre}</h2>
  <h3>Tienes {edad} años de edad</h3>
  </div>);
  return presentacion;
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>
          Proyecto de Auditoria.
        </h1>
        {HolaMundo("Fernando",39)}

        <p><span>{ GetDia() }</span></p>
        <a className="App-link" href="https://reactjs.org" title="Para aprender Reactjs">
          Hola, mundo en React JS!!
          </a>
      </header>
      <section>
      <MyComponente/>
      </section>
    </div>
  );
}

export default App;
